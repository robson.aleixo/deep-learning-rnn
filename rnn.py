
import pandas as pd 
import numpy as np
import os
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from keras.layers import Dense
from keras.layers import Dropout
from keras.models import Sequential
from keras.layers import LSTM
import keras


class RnnDeepLearning:
    '''
    main problem: It's used to predict based in severeal timesteps in the past
    example: Predict google stock with LSTM
    category: supervised
    '''
    def __init__(self, df):

        self.regressor = Sequential()
        self.df_train = df
        self.timestep = 60

        # Creating a data structure with 60 timesteps and 1 output        
        self.X_train = []
        self.y_train = []
        for i in range(self.timestep, self.df_train.shape[0]):
            self.X_train.append(self.df_train[i-self.timestep:i, 0])
            self.y_train.append(self.df_train[i, 0])

        self.X_train, self.y_train = np.array(self.X_train), np.array(self.y_train) 

        # Reshaping
        # Adding dimensions to include indicators in order to predict better
        # Preparing data to input structure of keras (observations, timesteps, indicators)
        self.X_train = np.reshape(self.X_train, 
                                    (self.X_train.shape[0], self.X_train.shape[1], 1))

    def build_regressor(self):

        # Adding the first LSTM layer and some Dropout regularisation
        self.regressor.add(LSTM(units = 50, # number of neurons (LSTM cells)
                            return_sequences = True, # It will be add more layers
                            input_shape = (self.X_train.shape[1], 1)))
        self.regressor.add(Dropout(0.2)) # 20% of the iterations will be ignored

        # Adding a second LSTM layer and some Dropout regularisation
        self.regressor.add(LSTM(units = 50, 
                            return_sequences = True))
        self.regressor.add(Dropout(0.2))

        # Adding a third LSTM layer and some Dropout regularisation
        self.regressor.add(LSTM(units = 50, 
                            return_sequences = True))
        self.regressor.add(Dropout(0.2))

        # Adding a fourth LSTM layer and some Dropout regularisation
        self.regressor.add(LSTM(units = 50))
        self.regressor.add(Dropout(0.2))

        # Adding the output layer
        self.regressor.add(Dense(units=1))

        # Compiling the RNN
        self.regressor.compile(optimizer = 'adam', loss = 'mean_squared_error')

        # Fitting the RNN to the Training set
        self.regressor.fit(self.X_train, self.y_train, 
                        epochs = 50, 
                        batch_size = 32 # number of times it will be wait to update
                        )

    def apply_prediction(self, pred, sc):

        prediction = self.regressor.predict(pred)
        prediction = sc.inverse_transform(prediction)

        return prediction

    def visualising_results(self, x, y):

        plt.plot(x, 
                    color='red', 
                    label = 'Real Google Stock Price')
        plt.plot(y)
        plt.title('Google Stock Price Prediction')
        plt.xlabel('Time')
        plt.ylabel('Google Stock Price')
        plt.legend()
        plt.show()

    def execute(self):

        self.build_regressor()

    def google_prediction(self):

        sc = MinMaxScaler(feature_range=(0,1))

        # Getting the real google stock price 
        file_test_path = os.path.join(file_path, 'Google_Stock_Price_Test.csv')

        # Preparing test data
        dataset_test = pd.read_csv(file_test_path)
        testing_set = dataset_test.iloc[:,1:2].values

        dataset_total = pd.concat((dataset_train['Open'], 
                                    dataset_test['Open']), axis = 0)

        # Getting data from 1st january until 31st january 
        inputs = dataset_total[len(dataset_total) - len(dataset_test['Open']) - self.timestep:].values
        inputs = inputs.reshape(-1, 1)
        inputs = sc.fit_transform(inputs)
        
        X_test = []
        for i in range(timesteps, 80):
            X_test.append(inputs[i-timesteps:i, 0])
        X_test = np.array(X_test)
        X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

        # Make the prediction
        prediction = self.apply_prediction(X_test, sc)

        # Visualising the results
        rnn.visualising_results(testing_set, prediction)

if __name__ == "__main__":

    '''
    In this case, the train and test data came in two files, the first part 
    we built the regressor and the second we get the test data with real outcomes
    and concatenate with the train data
    '''

    ###################################################
    # Part 1 - Buiding the reforce neural networking  #
    ###################################################

    timesteps = 60
    file_path = os.path.join(os.getcwd(), 'secao_3 - RNN')
    file_train_path = os.path.join(file_path, 'Google_Stock_Price_Train.csv')
    
    # Importing the dataset
    dataset_train = pd.read_csv(file_train_path)
    training_set = dataset_train.iloc[:,1:2].values
    
    # Feature Scaling
    sc = MinMaxScaler(feature_range=(0,1))
    training_set_scaled = sc.fit_transform(training_set)
    
    # Build the regressor
    rnn = RnnDeepLearning(training_set_scaled)

    rnn.execute()

    ###################################################
    # Part 2 - Making the prediction                  #
    ###################################################

    rnn.google_prediction()

    


    
